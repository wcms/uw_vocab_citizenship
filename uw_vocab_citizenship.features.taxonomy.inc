<?php

/**
 * @file
 * uw_vocab_citizenship.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function uw_vocab_citizenship_taxonomy_default_vocabularies() {
  return array(
    'uw_citizenship' => array(
      'name' => 'Citizenship',
      'machine_name' => 'uw_citizenship',
      'description' => '',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
